/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	index:function(req,res){
			User.find(function(err, user){
					if (err) {
						res.send(err);
					}
					else {
            var query = 'SELECT level FROM userrelation WHERE user='+req.session.passport.user;
            var query2 = 'SELECT * FROM user WHERE id='+req.session.passport.user;
            User.query(query, function(err, user){
              if(err){
                console.log(err);
              }
              else{
                console.log('nivel de acceso: '+user[0].level);
                if(user[0].level == 1){
                  console.log('entre en admin');
                  User.query(query2, function(err,user2){
                    if(err){
                      return console.log(err);
                    }
                    else{
                      return res.view('panel/administrador',{
                        usuarios: user2[0]
                      });
                    }
                  });
                }
                else{
                  console.log('entre en usuario');
                  User.query(query2, function(err, user){
                    if(err) return console.log(err);
                    else{
                      var queryNodo = 'SELECT * FROM user'+user[0].dni;
                      User.query(queryNodo, function(err, nodo){
                        if(err) return console.log(err);
                        else{
                          console.log('entre en nodo de usuario');
                          return res.view(' panel/usuario',{
                            usuarios:user[0],
                            nodo: nodo
                          });
                        }
                      });

                    }
                  });

                }
              }
            });
						//return res.view('panel/index');
					}
			});


	},

	create:function(req, res){
		var idUsuario;
		nombre = req.param('nombre');
		apellido = req.param('lastname');
		documento = req.param('dni');
		correo = req.param('email');
		pass = req.param('pass');
		estado = 0;
		moneda = 0;
		referido = req.param('referido');
		console.log(nombre);
		console.log(apellido);
		console.log(documento);
		console.log(correo);
		console.log(pass);
		//Recolectamos los datos del usuario y los unimos en un Objeto
		var userObject ={
			name: nombre,
			lastname: apellido,
			dni: documento,
			email: correo,
			password:pass,
			status:estado,
			coins:moneda
		}


		User.create(userObject, function(err,user){
			if(err){
				console.log(err);
			}
			else{
				query = "SELECT id FROM user where dni='"+documento+"'";
				query2 = "SELECT id FROM user where name='"+referido+"'";

				 User.query(query, function(err, user1){
					if (err) {
						console.log(err);
					}
					var idUser = user1[0].id;
					User.query(query2, function(err, user2){
						if (err) {
							console.log(err);
						}
						var idUserReferido = user2[0].id;

						var userRelativo = {
							user: idUser,
							userfather:idUserReferido,
							level:2
						}

						UserRelation.create(userRelativo, function(err, relacionado){
							if (err) {
								console.log(err);
							}
							else {
								console.log("Relacion exitosa");
							}
						});
					});
				});

				console.log(user);
			}
		});


		query3 = 'CREATE TABLE user'+documento+'(id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, numnode INT(6), usuariohijo INT(6) )';
		User.query(query3, function(err, results){
			if(err){
				console.log(err);
			}
			else{
					console.log('Tabla user'+documento+' creada exitosamente');
			}

		});
		res.send(nombre);
	},

	nuevo:function(req, res){
		return res.view('panel/nuevo');
	},

  nodo:function(req, res){
    var idUsuario = req.session.passport.user;
    var querydni= 'SELECT user.dni FROM user WHERE id='+req.session.passport.user;
    User.query(querydni, function(err, user){
      if(err) return console.log(err);
      else{

        var queryNodo = 'SELECT * FROM  user'+user[0].dni;
        console.log(queryNodo);
        Node.query(queryNodo, function(err, nodo){
          if(err) return console.log(err);
          else{

            return res.send(nodo);
          }
        });
      }
    });
  }

};
