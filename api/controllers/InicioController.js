/**
 * InicioController
 *
 * @description :: Server-side logic for managing inicios
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	inicio:function(req, res){
    if(req.session.passport == undefined){
      return res.redirect('/index.html');
    }
    else{
      return res.redirect('/user')
    }
  }
};

