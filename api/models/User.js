/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var bcrypt = require('bcryptjs');
module.exports = {

  attributes: {
    id:{
      type:'integer',
      unique:true,
      primaryKey:true
    },
    name:{
      type:'string',
      required:true
    },
    lastname:{
      type:'string',
      required:true
    },
    dni:{
      type:'integer',
      required:true,
      unique:true
    },
    email:{
      type:'email',
      required:true,
      unique:true
    },
    password:{
      type:'string',
      required:true
    },
    status:{
      type:'boolean'
    },
    coins:{
      type:'float'
    },

    toJSON: function() {
            var obj = this.toObject();
            delete obj.password;
            return obj;
          }
  },
  beforeCreate: function(user, cb) {
        bcrypt.genSalt(10, function(err, salt) {
            bcrypt.hash(user.password, salt, function(err, hash) {
                if (err) {
                    console.log(err);
                    cb(err);
                } else {
                    user.password = hash;
                    cb();
                }
            });
        });
    }
};
