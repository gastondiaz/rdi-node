
module.exports.routes = {

  '/':'InicioController.inicio',
  'post /login': 'AuthController.login',
  '/logout': 'AuthController.logout',
  'post /user/create': 'UserController.create',
  'get /user': 'UserController.index',
  'get /user/nuevo' : 'UserController.nuevo',
  'get /nodo':'UserController.nodo'
};
